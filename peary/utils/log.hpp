/**
 * @file
 * @brief Provides a logger and macros for convenient access
 */

#ifndef CARIBOU_LOG_H
#define CARIBOU_LOG_H

#ifdef WIN32
#define __func__ __FUNCTION__
#endif

#include <cstring>
#include <mutex>
#include <ostream>
#include <sstream>
#include <string>
#include <vector>

#include "logging.hpp"

#ifndef __FILE_NAME__
/**
 *  @brief Base name of the file without the directory
 */
#define __FILE_NAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif

/**
 * @brief Name of the log stream
 */
#ifdef PEARY_DEVICE_NAME
#define __LOG_NAME__ PEARY_DEVICE_NAME
#else
#define __LOG_NAME__ ""
#endif

/**
 * @brief Execute a block only if the reporting level is high enough
 * @param level The minimum log level
 */
#define IFLOG(level) if(caribou::LogLevel::level <= caribou::Log::getReportingLevel() && !caribou::Log::getStreams().empty())

/**
 * @brief Create a logging stream if the reporting level is high enough
 * @param level The log level of the stream
 */
#define LOG(level)                                                                                                          \
  if(caribou::LogLevel::level <= caribou::Log::getReportingLevel() && !caribou::Log::getStreams().empty())                  \
  caribou::Log().getStream(                                                                                                 \
    caribou::LogLevel::level, __FILE_NAME__, std::string(static_cast<const char*>(__func__)), __LINE__, __LOG_NAME__)

/**
 * @brief Create a logging stream that overwrites the line if the previous message has the same identifier
 * @param level The log level of the stream
 * @param identifier Identifier for this stream to determine overwrites
 */
#define LOG_PROGRESS(level, identifier)                                                                                     \
  if(caribou::LogLevel::level <= caribou::Log::getReportingLevel() && !caribou::Log::getStreams().empty())                  \
  caribou::Log().getProcessStream(identifier,                                                                               \
                                  caribou::LogLevel::level,                                                                 \
                                  __FILE_NAME__,                                                                            \
                                  std::string(static_cast<const char*>(__func__)),                                          \
                                  __LINE__,                                                                                 \
                                  __LOG_NAME__)

#define LOGTIME caribou::Log::getTimestamp()

#endif /* CARIBOU_LOG_H */
